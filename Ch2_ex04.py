#Exercise 4 assume that we execute the following assignment statements:

width = 17
height = 12.0

#for each of the following expressions, write the value of the expression and the type
#(of the value of the expression).

w1 = width//2
w2 = width/2.0
h1 = height/3
e = 1 + 2 * 5

#this will print out values and types
print("The value of width//2 is:",w1,"and the type is",type(w1))
print("The value of width/2 is:",w2,"and the type is",type(w2))
print("The value of height/3 is:",h1,"and the type is",type(h1))
print("The value of 1+2*5 is:",e,"and the type is",type(e))
