#Chapter 3
#Exercise 2: Rewrite your pay program using try and except so that your program handles non-numeric input
#gracefully by printing a message and exiting the program.
#The following shows two execution of the program

try:
    hours = int(input("Enter the hours: "))
    rate = float(input("Enter the rate: "))
except:
    print("please enter a numeric value")
    quit()
if hours > 40:
    pay = (hours - 40)*rate* 1.5 +40 * rate
else:
    pay = hours * rate
print("pay = ", pay)



