#Exercise 1: Revise a previous program as follows: Read and parse the "from" lines and pull out the addresses
#from the line. Count the number of messages from each person using a dictionary.
#after all the data has been read, print the person with the most commits by creating a list of (count, email)
#tuples from the dictionary. Then sort the list in reverse order and put the person who has the most commits.

#sample line:
#from stephen.Marquard@uct.ac.za Sat jan 5 09:14:16 2008

#Enter a file name: mbox-short.txt
#cwen@iupui.edu 5

#enter a file name: mbox.txt
#zqian@umich.edu 195

fname = input("Enter a file name: ")
fhand = open(fname, 'r')
email_addresses = {}

for line in fhand:
    if line.startswith("From "):
        email = line.split()[1]
        email_addresses[email] = email_addresses.get(email, 0) + 1

lst = []
for key, value in email_addresses.items():
    lst.append((value, key))

lst.sort(reverse=True)
person_tuple = lst[0]
print(person_tuple[1], person_tuple[0])
