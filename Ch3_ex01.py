#Chapter 3
#Exercise 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate
#for hours worked above 40 hours.
hours = int(input("Enter the hours: "))
rate = float(input("Enter the rate: "))
pay = hours * rate

if hours > 40:
    print(pay * 1.5)
else:
    print(pay)