#Exercise 6: Rewrite the program that prompts the user for lists of numbers and prints out the maximum
#and the minimum of the numbers at the end when the user enters "done". Write the program to store the
#numbers the user enters in a list and use the max() and min() functions to compute the maximum
#and the minimum numbers after the loop completes.

letter = []

while True:
    str_val = input("enter a number: ")
    if str_val == 'done':
        break
    try:
        val = float(str_val)
    except:
        print("invalid input")
        continue
    letter.append(val)

print(min(letter), max(letter))