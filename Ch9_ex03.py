#Exercise 3: Write a program to read through a mail log, build a histogram using a dictionary to count
#how many messages have come from each email address, and print the dictionary.

fname = input("Enter a file name: ")
fhand = open(fname,'r')
email_addresses = {}#open a dictionary with email addresses


for line in fhand:#iterate through the line
    if line.startswith("From "):
        email = line.split()[1]#we need the second word which is the email address
        email_addresses[email] = email_addresses.get(email, 0) + 1
print(email_addresses)