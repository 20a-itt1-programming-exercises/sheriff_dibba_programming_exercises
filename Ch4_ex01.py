#Exercise 1: Run the program on your system and see what numbers you get.
#run the program more than once and see what numbers you get.

#the random function is only one of many functions that handle random numbers.
#the function randint takes the parameters low and high, and returns an interger
#between low and high (including low and high)

import random

print(random.randint(5, 10))

t = [1, 2, 3]
print(random.choice(t))