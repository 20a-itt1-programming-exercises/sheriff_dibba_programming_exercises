#Exercise 2: Write a program that categorizes each mail message by which day of the week the commit was done.
#to do this look for lines that start with "from",then look for the third word
#and keep a running count of each of the days of the week. At the end of the program
#print out the contents of your dictionary(order does not matter).
#sample line
#from stephan.marquard@uct.ac.za Sat Jan 5 09:14:16 2008
#sample execution:
#python dow.py
#enter a file name: mbox-short.txt
#{'fri':20, 'Thu': 6, 'Sat': 1}

fname = input("Enter a file name: ")
fhand = open(fname,'r')#open function and in read mode
days = {}#declare a dictionary (empty)

for line in fhand:
    if line.startswith("From "):#check all the lines that start with from
        day = line.split()[2]# the day is they and a value will be number of times the day is repeated
        days[day] = days.get(day, 0) + 1 # using get function with default value 0, will check the day and if not found it will be store as 0
#the value will be saved in days[day]
print(days)