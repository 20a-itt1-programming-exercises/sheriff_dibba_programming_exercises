#Excercise 4: Add code the above program to figure out who has the most messages in the file.
#After all the data has been read and the dictionary has been created, look through the dictionary.
#using a maximum loop(see Chapter 5: maximun and minimum loops) who has the most messages
#and print how many messages the person has.


fname = input("Enter a file name: ")
fhand = open(fname,'r')
email_addresses = {}#open a dictionary with email addresses

max_address = None #declare 2 variables
max_emails = 0

for line in fhand:
    if line.startswith("From "):
        email = line.split()[1]
        email_addresses[email] = email_addresses.get(email, 0) + 1

for k in email_addresses:
    if email_addresses[k] > max_emails:
        max_address = k
        max_emails = email_addresses[k]
print(max_address, max_emails)
