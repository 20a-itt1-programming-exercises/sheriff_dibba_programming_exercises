#Exercise 5: Write a program to read through the mail box data and when you find line that starts
#with "from", you will split the line into words using the split function. We are interested in who
#sent the message, which is the second word on the from line.

filename = input("enter file name: ")
count = 0

fhand = open(filename, 'r') #opens up the file and in read mode
for line in fhand:
    if line.startswith('From:'): #list of all the words after from
        print(line.split(' ')[1]) #this will give us the word index after from which the email address
        count += 1

print("There were", count, "lines in the file with from as the first word")