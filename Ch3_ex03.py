#Chapter 3
#Exercise 3: Write a program to prompt for a score between 0.0 and 1.0. if the score is out of range,
#print an error message. if the score is between 0.0 and 1.0, print a grade using the following table:

try:
    score = float(input("Enter score: "))
except:
    print("Bad score")
    quit()
if score == 0.95:
    print("A")
elif score == 10.0:
    print("Bad score")
elif score == 0.75:
    print("C")
elif score == 0.5:
    print("F")