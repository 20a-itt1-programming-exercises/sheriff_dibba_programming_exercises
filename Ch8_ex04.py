#Exercise 4: Download a copy of the file www.py4e.com/code3/romeo.txt.
#write a program to open the file romeo.txt and read it line by line.
#for each line, split the line into a list of words using the split function.
#for each word check to see if the word is already in the list.
#if the word is in not in th elist, add it ot the list. When the program completes
#sort and print the resulting words in alphabetical order.

words = []

fhand = open('romeo.txt', 'r')
for line in fhand:
    for word in line.split(' '):
        if word not in words:
            words.append(word)

print(sorted(words))