#Exercise 5: This program records the domain name(instead of the address) where the message was sent from
#instead of who the mail came from(i.e., the whole email address). At the end of the program,
#print out the contents of your dictionary.

#python schoolcount.py
#Enter a file name: mbox-short.txt
#{'media.berkeley.edu': 4, 'uct.ac.za': 6, 'umivh.edu': 7, 'gmail.com': 1, 'caret.cam.ac.uk': 1, 'inpui-edu': 8}

fname = input("Enter a file name: ")
fhand = open(fname, 'r')
domains = {}#open a dictionary with domains


for line in fhand:#iterate through the line
    if line.startswith("From "):
        email = line.split()[1]#we need the just the domain
        domain = email.split("@")[1]#we split the second statment and pick name after @
        domains[domain] = domains.get(domain, 0) + 1

print(domains)
